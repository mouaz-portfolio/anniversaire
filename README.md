# Application C# - Anniversaire ~ Mouaz MOHAMED

Anniversaire est une application console développée en C# qui permet de gérer des anniversaires et les données relatives aux anniversaires des personnes

---

## Diagramme des cas d'utilisation (User Case Diagram) de mon application
```plantuml
left to right direction
:Utilisateur: as Utilisateur
package <uc>Anniversaire{
    Utilisateur --- (Rechercher des personnes plus âgées que l'âge qu'on a spécifié)
    (Rechercher des personnes plus âgées que l'âge qu'on a spécifié) <.. (Afficher la liste des personnes trouvées) : « include »
}
```

---

## Modèle Conceptuel de Données
```plantuml

class Anniversaire {
    - id: int
    - nom: string
    - prenom: string
    - prenomPseudonyme: string
    - nomPseudonyme: string
    - dateAnniversaire: DateTime
    - anniversaire: DateTime
    + GetdateAnniversaire(): DateTime
    + GetId(): int
    + GetNom(): string
    + GetnomPseudonyme(): string
    + GetprenomPseudonyme(): string
    + Anniversaire(anniversaire: DateTime, prenomPseudonyme: string, nomPseudonyme: string, prenom: string, nom: string)
    + ToString(): string
}

class ConnectionJoyeuxAnniversaire {
    - sConnection: string
    - connection: MySqlConnection
    - command: MySqlCommand
    + GetConnection(): MySqlConnection
}

class Program {
    + Main(args: string[]): void
}

Anniversaire "1" --> "1" ConnectionJoyeuxAnniversaire
Program --> "1" ConnectionJoyeuxAnniversaire : Uses
```

---

## Structure de l'application

Cette application est organisé en trois fichiers principaux :

- `Anniversaire.cs`: Ce fichier contient la définition de la classe `Anniversaire`, qui représente les données d'un anniversaire.
- `ConnectionJoyeuxAnniversaire.cs`: Ce fichier contient une classe statique `ConnectionJoyeuxAnniversaire` pour gérer la connexion à la base de données.
- `Program.cs`: Ce fichier contient la classe principale `Program`, qui démontre l'utilisation des fonctionnalités du projet.

---

## Classe Anniversaire

La classe `Anniversaire` représente les données d'un anniversaire, avec les attributs suivants :

- `id`: Identifiant de l'anniversaire.
- `nom`: Nom de la personne.
- `prenom`: Prénom de la personne.
- `prenomPseudonyme`: Prénom pseudonyme de la personne.
- `nomPseudonyme`: Nom pseudonyme de la personne.
- `dateAnniversaire`: Date de naissance de la personne.
- `anniversaire`: Date d'anniversaire de la personne.

Elle fournit des méthodes pour récupérer les valeurs de ses attributs, ainsi qu'un constructeur pour initialiser ses valeurs.

---

## Classe ConnectionJoyeuxAnniversaire

La classe `ConnectionJoyeuxAnniversaire` est une classe statique qui gère la connexion à la base de données. Elle utilise la bibliothèque MySQL.Data pour la connexion à une base de données MySQL. Elle fournit une méthode statique `GetConnection()` pour obtenir la connexion à la base de données.

---

## Classe Program

La classe `Program` contient la méthode principale `Main()` qui démontre l'utilisation des fonctionnalités du projet. Actuellement, elle permet de rechercher les personnes ayant un âge spécifique et d'afficher leurs détails.

---

## Code source : Anniversaire.cs
```csharp
using System;

namespace TpAdo
{
    class Anniversaire
    {
        // Attributs privés de la classe Anniversaire
        private int id;
        private string nom;
        private string prenom;
        private string prenomPseudonyme;
        private string nomPseudonyme;
        private DateTime dateAnniversaire;
        private DateTime anniversaire;

        // Méthodes pour récupérer les valeurs des attributs
        public DateTime GetdateAnniversaire()
        {
            return this.dateAnniversaire;
        }

        public int GetId()
        {
            return this.id;
        }

        public string GetNom()
        {
            return this.nom;
        }

        public string GetnomPseudonyme()
        {
            return this.nomPseudonyme;
        }

        public string GetprenomPseudonyme()
        {
            return this.prenomPseudonyme;
        }

        // Constructeur paramétré de la classe Anniversaire
        public Anniversaire(DateTime anniversaire, string prenomPseudonyme, string nomPseudonyme, string prenom, string nom)
        {
            this.anniversaire = anniversaire;
            this.prenomPseudonyme = prenomPseudonyme;
            this.nomPseudonyme = nomPseudonyme;
            this.prenom = prenom;
            this.nom = nom;
        }

        // Redéfinition de la méthode ToString pour afficher les détails de l'anniversaire
        public new string ToString()
        {
            return string.Format("Le nom : {0}, le prenom {1}et la date de naissance : {2}", this.nomPseudonyme, this.prenomPseudonyme, this.dateAnniversaire);
        }

    }
}
```

## Code source : ConnectionJoyeuxAnniversaire.cs
```csharp
using MySql.Data.MySqlClient;
using System;

namespace TpAdo
{
    // Classe statique pour gérer la connexion à la base de données
    static class ConnectionJoyeuxAnniversaire
    {
        static string sConnection;
        static MySqlConnection connection;
        static MySqlCommand command;

        // Constructeur statique pour initialiser la connexion
        static ConnectionJoyeuxAnniversaire()
        {
            // Chaîne de connexion à la base de données
            sConnection = "user=root;password=yh18ev34&*;server=localhost;database=Anniv";
            connection = new MySqlConnection(sConnection);
        }

        // Méthode pour obtenir la connexion à la base de données
        public static MySqlConnection GetConnection()
        {
            return ConnectionJoyeuxAnniversaire.connection;
        }
    }
}
```

## Code source : Program.cs 
```csharp
using System;
using TpAdo;

namespace TpAdo
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Console.WriteLine( "Jour : ");
            int jour = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Mois : ");
            int mois = Convert.ToInt32(Console.ReadLine());

            TableAnniversaire ta = new TableAnniversaire();
            List<Anniversaire> resultat = ta.QuiEstNeCeJour(jour, mois);

            foreach(Anniversaire a in resultat)
            {
                Console.WriteLine(a.GetnomPseudonyme());
                Console.WriteLine(a.GetprenomPseudonyme());
                Console.WriteLine(a.GetdateAnniversaire());
            }*/

            // Rechercher des personnes plus âgées
            Console.WriteLine("Saisissez un âge : ");
            int ageSaisi = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Personnes plus âgées que " + ageSaisi + " ans :");
            int ageCalcul = Convert.ToInt32(Console.ReadLine());


            TableAnniversaire ta = new TableAnniversaire();
            List<Anniversaire> personnesPlusAgees = ta.PersonnesPlusAgeesQue(ageSaisi);

            // Affichage des personnes plus âgées que l'âge spécifié
            foreach (Anniversaire personne in personnesPlusAgees)
            {
                Console.WriteLine(personne.ToString());   
            }

            /*TableAnniversaire tableAnniversaire = new TableAnniversaire();
            Console.WriteLine("Il y a {0} occurences dans la table anniversaire",
                tableAnniversaire.CompteOccurence());*/
        }
    }
}
```